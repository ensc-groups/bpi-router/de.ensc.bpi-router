## requires INHERIT += "elito-emit-buildvars" from the de.sigma-chemnitz
## layer
DTREE_DIR := $(abspath $(dir $(lastword $(MAKEFILE_LIST))))

TOPDIR ?= ${DTREE_DIR}/../../../../../bsp
DEPLOY_DIR ?= ${TOPDIR}/build/deploy
MACHINE ?= bananapi-m2b
GZIP ?= gzip

include ${DEPLOY_DIR}/buildvars/${MACHINE}/elito-devicetree.mk

export PATH := ${BUILDVAR_PATH}:${PATH}
export SOC := generic

export KERNEL_DIR = $(abspath ${WS}/kernel-${KERNEL_V})
export KERNEL_DTREE_DIR = ${KERNEL_DIR}/arch/arm64/boot/dts

export DTC_INC_PATH = $(dir ${BUILDVAR_OE_KERNEL_FILES})dtree
export DTC_CPPFLAGS = -I ${DTC_INC_PATH}
export MAKE

NOW := $(shell date +'%Y-%m-%d %H:%M:%S')

KERNEL_IMAGE ?= ${KERNEL_BUILD_DIR}/arch/arm64/boot/Image
INITRD_IMAGE ?= ${DEPLOY_DIR}/images/${MACHINE}/initrd-${MACHINE}.cpio.gz

VPATH += ${DTC_INC_PATH}

DTBS = \
	${MACHINE}.dtb \

SED ?= sed
SED_ITS = ${SED} \
	-e 's!@KERNEL_IMAGE@!${KERNEL_IMAGE}!g' \
	-e 's!@INITRD_IMAGE@!${INITRD_IMAGE}!g' \
	-e 's!@MACHINE@!${MACHINE}!g' \

## options which are whitelisted for hash generation
SED_ITS_EXTRA = \
	-e 's!@DATE@!${NOW}!g' \

MKIMAGE ?= mkimage

O ?= ${WS}/.dtb/${KERNEL_V}

export DTC_EXTRA_FLAGS = -@

build-dtree:	$(addprefix $O/,${DTBS})

clean:	FORCE
	rm -f $O/* $O/.*

$O/.build-dtree-%:	%.dts FORCE
	+env srcdir='${<D}' elito-build-dtree -s -o $O ${<F} $*:$(CPU):1

$O/.build-dtree_o-%:	%.dts FORCE
	+env srcdir='${<D}' elito-build-dtree -s -o $O ${<F} $*:$(CPU):1 --overlay

$O/%.dtb:	$O/.build-dtree-%
	rm -f ${WS}/www/oftree-$*.dtb ${WS}/www/oftree-$*-${KERNEL_V}.dtb
	install -p -m 0644 $@ ${WS}/www/oftree-$*-${KERNEL_V}.dtb
	ln -s oftree-$*-${KERNEL_V}.dtb ${WS}/www/oftree-$*.dtb

$O/%.dtbo:	$O/.build-dtree_o-%
	@:

$O/%.fit:	$O/%.its $(addprefix $O/,${DTBS}) ${KERNEL_IMAGE} ${INITRD_IMAGE}
	${MKIMAGE} -f $(filter %.its,$^) $@

$O/%.fit.gz:	$O/%.fit
	@rm -f '$@'
	${GZIP} -c < '$<' > '$@'

_squote = '#'
_its_sed_hash := $(shell printf '%s' '$(subst ${_squote},XX,${SED_ITS})' | md5sum | awk '{print $$1}')

_its_cpp_flags =				\
	 -D__DTS__				\
	-x assembler-with-cpp			\
	-nostdinc -undef			\
	-Wp,-MD,${@D}/.${@F}.d			\

## XXX: this does not work.  WHY???
$O/.its-hash.%.${_its_sed_hash}:	%.its.in
	@rm -f '${@D}/.its-hash.$*'.*
	@touch $@

$O/%.its:	$O/%.its.pre
	$(CPP) ${_its_cpp_flags} - < $< -o $@

$O/%.its.pre:	%.its.in $O/.its-hash.%.${_its_sed_hash} ${KERNEL_IMAGE}
	@rm -f $@
	${SED_ITS} ${SED_ITS_EXTRA} < $< > $@
	@chmod a-w $@

shell:
	bash -l

FORCE:
.PHONY:	FORCE
.SECONDARY:





## requires INHERIT += "elito-emit-buildvars" from the de.sigma-chemnitz
## layer
DTREE_DIR := $(abspath $(dir $(lastword $(MAKEFILE_LIST))))
SETUP ?= default
DEPLOY_DIR ?= ${DTREE_DIR}/../../../../build/tmp/${SETUP}/deploy
MACHINE ?= bananapi-m2b

export MAKE

include ${DEPLOY_DIR}/buildvars/${MACHINE}/elito-devicetree.mk

export PATH := ${BUILDVAR_PATH}:${PATH}
export SOC := generic
#export KERNEL_DIR = ${DTREE_DIR}/../../../../workspace/kernel.m2b
#export KERNEL_DTREE_DIR = ${KERNEL_DIR}/arch/arm/boot/dts

O ?= ${WS}/.dtb/${KERNEL_V}

build-dtree:	$O/${MACHINE}.dtb

.build-dtree-%:	%.dts FORCE
	+elito-build-dtree -s -o $O $(filter %.dts,$^) $*:$(CPU):1

$O/%.dtb:	.build-dtree-%
	@:

FORCE:
.PHONY: FORCE
