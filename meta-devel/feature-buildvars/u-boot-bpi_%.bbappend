BUILDVARS_EMIT = "1"

BUILDVARS_EXPORT += "\
  ARCH \
  TARGET \
  KERNEL_AR \
  KERNEL_CC \
  KERNEL_LD \
  PYTHON \
  STAGING_KERNEL_DIR \
  STAGING_INCDIR_NATIVE \
  STAGING_LIBDIR_NATIVE \
  UBOOT_CC \
  UBOOT_LD \
"
