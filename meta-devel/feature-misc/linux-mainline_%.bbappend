python() {
    rev = d.getVar('KERNEL_REVISION', False)
    if rev == '${AUTOREV}':
        d.setVar('PATCHSET', '')
}
