URI_PROJECT_BASE = "git+file://${WS_DIR}${@['','/.repo'][oe.data.typed_value('DEVREV_SPLITTED', d)]}"
URI_PROJECT_TOOLS = "${URI_PROJECT_BASE}"
URI_PROJECT_PRIV = "${URI_PROJECT_BASE}"

DEVREV_SPLITTED ?= "false"
DEVREV_SPLITTED[type] = "boolean"

SRCREV:forcevariable = "${AUTOREV}"
SRCREV_FORMAT = ""

python do_warn_devrevs() {
    bb.warn("using devel-revs")
}
addtask do_warn_devrevs after do_unpack before do_build
