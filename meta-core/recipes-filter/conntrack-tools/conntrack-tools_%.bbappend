PACKAGES =+ "${PN}-conntrackctl"

FILES:${PN}-conntrackctl = "${sbindir}/conntrack ${sbindir}/nfct"

PACKAGECONFIG[systemd] = "--enable-systemd,--disable-systemd,systemd"

PACKAGECONFIG:append = " systemd"
