LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://COPYING;md5=1ebbd3e34237af26da5dc08a4e440464"
SUMMARY = "Builds iptable rulesets"
HOMEPAGE = "https://gitlab.com/ensc-groups/bpi-router/tools/iptables-builder"
SECTION = "net"

SRC_URI = "\
  ${URI_PROJECT_TOOLS}/iptables-builder \
"
SRCREV = "1bce5800aac7ca4937bcbcf12ad0a46d9e9f9550"

S = "${WORKDIR}/git"

inherit autotools

EXTRA_OEMAKE = "\
  -f ${S}/Makefile \
"

RDEPENDS:${PN} += "bash"

BBCLASSEXTEND += "native nativesdk"
