LICENSE = "GPL-3.0-or-later"
LIC_FILES_CHKSUM="file://COPYING;md5=1ebbd3e34237af26da5dc08a4e440464"
SUMMARY = "logger of sensor data captured by camera"
HOMEPAGE = "https://gitlab.com/ensc-groups/bpi-router/tools/cam-logger"
SECTION = "net"

SRC_URI = "\
    ${URI_PROJECT_TOOLS}/cam-logger;branch=master \
    ${CRATES} \
    file://app.css \
    file://app.js \
    file://chunk-vendors.js \
"
SRCREV = "70b8f9a81fdbc5f6e207560d95c0d303007d28b5"
S = "${WORKDIR}/git"
B = "${S}"

require ${BPN}-crates.inc

inherit cargo systemd cargo-fixup-frozen useradd

DEPENDS += "sqlite3 rrdtool tesseract"

oe_cargo_fix_env:append() {
        ##
	export pkgdatadir='${datadir}/${BPN}'
}

do_install:append() {
    mv ${D}${bindir} ${D}${sbindir}
    install -D -p -m 0644 ${S}/contrib/cam-logger.service ${D}${systemd_system_unitdir}/cam-logger.service
    install -D -p -m 0644 ${S}/contrib/cam-logger.socket  ${D}${systemd_system_unitdir}/cam-logger.socket

    install -D -p -m 0644 ${WORKDIR}/app.css          ${D}${datadir}/cam-logger/assets/app.css
    install -D -p -m 0644 ${WORKDIR}/app.js           ${D}${datadir}/cam-logger/assets/app.js
    install -D -p -m 0644 ${WORKDIR}/chunk-vendors.js ${D}${datadir}/cam-logger/assets/chunk-vendors.js
}

SYSTEMD_PACKAGES += "${PN}-socket"
USERADD_PACKAGES += "${PN}"

GROUPADD_PARAM:${PN} = "--system cam-logger"
USERADD_PARAM:${PN} = "--system --home / -M -g cam-logger cam-logger"

PACKAGE_BEFORE_PN += "${PN}-socket"

SYSTEMD_AUTO_ENABLE:${PN} = "disable"
SYSTEMD_SERVICE:${PN} = "${BPN}.service"

SYSTEMD_AUTO_ENABLE:${PN}-socket = "enable"
SYSTEMD_SERVICE:${PN}-socket = "${BPN}.socket"

RDEPENDS:${PN}-socket = "${PN} (= ${EXTENDPKGV})"
FILES:${PN}-socket = "${systemd_system_unitdir}/${BPN}.socket"
