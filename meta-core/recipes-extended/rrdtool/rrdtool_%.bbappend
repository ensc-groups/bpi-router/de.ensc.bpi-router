FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "\
    file://0001-rrd_open-use-O_CLOEXEC.patch \
    file://0002-rrd_open-use-blocking-F_SETLKW.patch \
    file://e59f703bbcc0af949ee365206426b6394c340c6f.patch \
"

RRECOMMENDS:${PN} += "\
    ttf-dejavu-sans-mono \
"
