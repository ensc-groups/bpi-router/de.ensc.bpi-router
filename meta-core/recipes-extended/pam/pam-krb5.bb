LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://LICENSE;md5=a03bfa27d91da14863a7027f861df764"

SRC_URI = "\
    git+https://github.com/rra/pam-krb5.git;branch=main \
"

PV = "4.11"
SRCREV = "54deebe6a6f2ec177ac3669391c424d5ea96a718"

S = "${WORKDIR}/git"

inherit autotools
inherit features_check

EXTRA_OEMAKE += "\
    dist_man_MANS= \
    pamdir=${nonarch_libdir}/security \
"

REQUIRED_DISTRO_FEATURES = "pam"

DEPENDS += "krb5 libpam"

FILES:${PN} += "${nonarch_libdir}/security"
