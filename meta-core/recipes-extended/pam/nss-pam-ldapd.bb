LICENSE = "LGPL-2.1-or-later"
LIC_FILES_CHKSUM = "file://COPYING;md5=fbc093901857fcd118f065f900982c24"

SRC_URI = "\
    git+https://github.com/arthurdejong/nss-pam-ldapd.git \
    file://nslcd.service \
"

PV = "0.9.12"
SRCREV = "33cf91cbbcc92c10fc99c828f9d9b94752299800"

S = "${WORKDIR}/git"

inherit autotools python3native systemd
inherit features_check
inherit useradd

DEPENDS += "openldap"

REQUIRED_DISTRO_FEATURES = "${@bb.utils.filter('PACKAGECONFIG', 'pam', d)}"

EXTRA_OECONF += "\
    --with-ldap-conf-file=${sysconfdir}/nslcd.conf \
    --with-pam-seclib-dir=${nonarch_base_libdir}/security \
    PYTHON=${bindir}/python3 \
"

#PACKAGECONFIG ??= "pam nss utils nslcd pynslcd sasl kerberos"
PACKAGECONFIG ??= "nss nslcd"

PACKAGECONFIG[nss] = "--enable-nss,--disable-nss"
PACKAGECONFIG[pam] = "--enable-pam,--disable-pam,libpam"
PACKAGECONFIG[utils] = "--enable-utils,--disable-utils,python3"
PACKAGECONFIG[nslcd] = "--enable-nslcd,--disable-nslcd"
PACKAGECONFIG[pynslcd] = "--enable-pynslcd,--disable-pynslcd"
PACKAGECONFIG[sasl] = "--enable-sasl,--disable-sasl,krb5"
PACKAGECONFIG[kerberos] = "--enable-kerberos,--disable-kerberos,krb5"

do_install:append() {
	install -D -p -m 0644 ${WORKDIR}/nslcd.service ${D}${systemd_system_unitdir}/nslcd.service
}

PACKAGE_BEFORE_PN = "\
    ${PN}-config \
    ${PN}-nss \
    ${PN}-pam \
    ${PN}-nslcd \
    ${PN}-utils \
    ${PN}-python \
"

FILES:${PN}-config = "${sysconfdir}/nslcd.conf"
RPROVIDES:${PN}-config = "virtual/nss-ldap-config"

FILES:${PN}-nslcd = "${sbindir}/nslcd"
RSUGGESTS:${PN}-nslcd = "virtual/nss-ldap-config"

FILES:${PN}-utils = "\
    python3 \
    ${bindir}/* \
    ${datadir}/nslcd-utils/* \
"

FILES:${PN}-python = "\
    python3 \
    ${datadir}/pynslcd \
"

FILES:${PN}-nss = "${libdir}/libnss_ldap.so.*"
RDEPENDS:${PN}-nss += "${PN}-nslcd"

FILES:${PN}-pam = "${nonarch_base_libdir}/security/pam_ldap.so"
RDEPENDS:${PN}-pam += "${PN}-nslcd"

SYSTEMD_PACKAGES = "${PN}-nslcd"
SYSTEMD_SERVICE:${PN}-nslcd = "nslcd.service"

USERADD_PACKAGES = "${PN}-nslcd"
GROUPADD_PARAM:${PN}-nslcd = "--system nslcd"
USERADD_PARAM:${PN}-nslcd = "--system --no-create-home --home-dir / --shell /bin/false nslcd"
