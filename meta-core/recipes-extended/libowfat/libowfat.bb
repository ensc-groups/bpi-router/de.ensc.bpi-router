PV = "0.33"
LICENSE = "GPL-2.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=8ca43cbc842c2336e835926c2166c28b"

SRC_URI = "\
    http://www.fefe.de/libowfat/libowfat-0.33.tar.xz \
    file://same-type.patch \
"

SRC_URI[sha256sum] = "311ec8b3f4b72bb442e323fb013a98f956fa745547f2bc9456287b20d027cd7d"

EXTRA_OEMAKE = "\
    -f '${S}/Makefile' \
    VPATH='${S}' \
    DIET= \
    CC='${BUILD_CC}' \
    CFLAGS='${CFLAGS} -fPIC -I${S}' \
    LDFLAGS='${LDFLAGS} -fpie' \
    AR='${AR}' \
    RANLIB='${RANLIB}' \
    CCC='${CC}' \
    prefix='${prefix}' \
    LIBDIR='${libdir}' \
    INCLUDEDIR='${includedir}' \
    MAN3DIR='${mandir}/man3' \
"

LTO ?= "-flto -ffat-lto-objects -fuse-linker-plugin ${LTOEXTRA}"

SELECTED_OPTIMIZATION:append = " ${LTO}"
TARGET_LDFLAGS:append:class-target = " ${LTO}"

# B = "${WORKDIR}/build"

PARALLEL_MAKE = ""

do_compile() {
	oe_runmake
}

do_install() {
	oe_runmake install DESTDIR='${D}'
}

BBCLASSEXTEND = "native"
