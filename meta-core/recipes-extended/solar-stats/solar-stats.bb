LICENSE = "GPL-3.0-or-later"
LIC_FILES_CHKSUM="file://COPYING;md5=1ebbd3e34237af26da5dc08a4e440464"
SUMMARY = "monitoring of solar cell systems"
HOMEPAGE = "https://gitlab.com/ensc-groups/bpi-router/tools/solar-stats"
SECTION = "net"

SRC_URI = "\
    ${URI_PROJECT_TOOLS}/solar-stats;branch=master \
    ${CRATES} \
"
SRCREV = "0205c420c910f44e688d31cd461d602bc04d1993"
S = "${WORKDIR}/git"
B = "${S}"

require ${BPN}-crates.inc

inherit cargo systemd cargo-fixup-frozen useradd

DEPENDS += "rrdtool sqlite3"

do_install:append() {
    mv ${D}${bindir} ${D}${sbindir}
    install -D -p -m 0644 ${S}/contrib/solar-stats@.service ${D}${systemd_system_unitdir}/${PN}@.service
}

SYSTEMD_AUTO_ENABLE = "disable"
SYSTEMD_SERVICE:${PN} = "${PN}@.service"

USERADD_PACKAGES += "${PN}"

GROUPADD_PARAM:${PN} = "--system solar-stats"
USERADD_PARAM:${PN} = "--system --home / -M -g solar-stats solar-stats"
