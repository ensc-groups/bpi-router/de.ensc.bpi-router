python() {
    deps = d.getVar('DEPENDS').split()
    try:
        deps.remove('libtirpc')
        deps.append('${PREFERRED_PROVIDER_libtirpc}')
        d.setVar('DEPENDS', ' '.join(deps))
    except:
        pass
}
