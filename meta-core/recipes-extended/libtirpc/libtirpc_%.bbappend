_flavor = "plain"
_flavor:class-target = "${@bb.utils.contains('DISTRO_FEATURES', 'rpcgss', 'rpcgss', 'plain', d)}"

require libtirpc-${_flavor}.inc

BPN = "libtirpc"

PN = "libtirpc-core"
PROVIDES = "libtirpc"
