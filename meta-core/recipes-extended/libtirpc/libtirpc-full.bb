LICENSE = "MIT"

inherit nopackages

DEPENDS = "libtirpc-core"

DEPENDS:append:class-target = "\
    ${@bb.utils.contains('DISTRO_FEATURES', 'rpcgss', 'krb5', '', d)} \
"

# BBCLASSEXTEND = "native nativesdk"

PROVIDES = "libtirpc"
