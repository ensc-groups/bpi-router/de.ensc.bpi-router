## --*- bitbake -*--

PACKAGECONFIG:append = "${@bb.utils.contains('DISTRO_FEATURES', 'rpcgss', ' gssapi', '', d)}"

PACKAGECONFIG[gssapi] = "${OECONF_KRB5},--disable-gssapi,krb5-initial"

## workaround broken libtool which reorders arguments
CC:append = " -Wl,--as-needed"

CFLAGS:append = " -I=/opt/krb5/include"
LDFLAGS:append = " -Wl,-as-needed -L=/opt/krb5/lib"

OECONF_KRB5="\
    --enable-gssapi \
    KRB5_CONFIG=${STAGING_DIR_TARGET}/opt/krb5/bin/krb5-config \
"

RRECOMMENDS:${PN}-dev[nodeprrecs] = "1"
RRECOMMENDS:${PN}-dev += "glibc-dev krb5-dev"

ASSUME_SHLIBS += "\
    libgssapi_krb5.so.2:libgssapi-krb5 \
    libkrb5.so.3:libkrb5 \
    libk5crypto.so.3:libk5crypto \
    libcom_err.so.2:libcomerr \
"
