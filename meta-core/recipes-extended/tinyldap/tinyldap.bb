LICENSE = "CLOSED"

SRC_URI = "\
    git+file://${WS_DIR}/.repo/tinyldap.git \
    file://tinyldap.service.in \
    file://tinyldap.socket.in \
"

SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"
B = "${WORKDIR}/build"

EXTRA_OEMAKE += "\
    -f '${S}/Makefile' \
    VPATH='${S}' \
    DIET= \
    CC='${CC}' \
    CFLAGS='${CFLAGS} -I${S}' \
    LDFLAGS='${LDFLAGS}' \
    LIBS="$libs \$(MBEDTLS_LIBS)" \
    ${PACKAGECONFIG_CONFARGS} \
"

LTO ?= "-flto -ffat-lto-objects -fuse-linker-plugin ${LTOEXTRA}"

SELECTED_OPTIMIZATION:append = " ${LTO}"
TARGET_LDFLAGS:append:class-target = " ${LTO}"

DEPENDS += "libowfat openssl libxcrypt"

PACKAGECONFIG ??= "${@'mbedtls' if d.getVar('CLASSOVERRIDE') == 'class-target' else ''}"

PACKAGECONFIG[mbedtls] = "USE_MBEDTLS=t,USE_MBEDTLS= MBEDTLS_LIBS=,mbedtls"

inherit pkgconfig useradd systemd

SED_CMD_PLAIN = "sed \
    -e 's!@TINYLDAP@!${sbindir}/tinyldap!g' \
    -e 's!@PORT@!389!g' \
"

SED_CMD_TLS = "sed \
    -e 's!@TINYLDAP@!${sbindir}/tinyldap-tls!g' \
    -e 's!@PORT@!636!g' \
"

do_compile() {
	libs=$(pkg-config --libs openssl libcrypt)

	oe_runmake

	for s in service socket; do
	    ${SED_CMD_PLAIN} ${WORKDIR}/tinyldap.$s.in > tinyldap.$s
	    ${SED_CMD_TLS}   ${WORKDIR}/tinyldap.$s.in > tinyldaps.$s
	done
}

UTILS = "\
    acl addindex asn1dump bindrequest dumpacls dumpidx \
    idx2ldif md5password mysql2ldif parse \
"

BINS = "\
    ldapclient ldapclient_str ldapdelete \
"

do_install() {
	install -D -p -m 0644 tinyldap.socket  ${D}${systemd_system_unitdir}/tinyldap.socket
	install -D -p -m 0644 tinyldap.service ${D}${systemd_system_unitdir}/tinyldap.service

	install -D -p -m 0755 tinyldap_systemd ${D}${sbindir}/tinyldap

	if ${@bb.utils.contains('PACKAGECONFIG', 'mbedtls', 'true', 'false', d)}; then
		install -D -p -m 0644 tinyldaps.socket  ${D}${systemd_system_unitdir}/tinyldaps.socket
		install -D -p -m 0644 tinyldaps.service ${D}${systemd_system_unitdir}/tinyldaps.service

		install -D -p -m 0755 tinyldap-tls_systemd ${D}${sbindir}/tinyldap-tls
	fi

	for b in ${BINS}; do
		install -D -p -m 0755 "$b" ${D}${bindir}/"$b"
	done

	for b in ${UTILS}; do
		install -D -p -m 0755 "$b" ${D}${libexecdir}/tinyldap/"$b"
	done
}

PACKAGE_BEFORE_PN = "${PN}-client ${PN}-utils"

USERADD_PACKAGES += "${PN}"

GROUPADD_PARAM:${PN} = "--system tinyldap"
USERADD_PARAM:${PN} = "--system --home / -M -g tinyldap tinyldap"

SYSTEMD_SERVICE:${PN} = "tinyldap.socket tinyldaps.socket"

FILES:${PN}-client = "${bindir}/ldap*"
FILES:${PN}-utils = "${libexecdir}/tinyldap/*"
FILES:${PN} = "${sbindir}/*"

BBCLASSEXTEND = "native"
