IMAGE_FEATURES += "read-only-rootfs devel-sshkey devel-history"
IMAGE_FEATURES += "ssh-server-openssh"
IMAGE_FEATURES += "ns-server krb5-server ldap-server"
IMAGE_FEATURES += "dhcp-server asterisk"

hostname = "gw-0.bigo.ensc.de"

inherit selinux-image
include bpi-image-common.inc

IMAGE_INSTALL += "\
    packagegroup-bpi-router \
"

BAD_RECOMMENDATIONS += "\
    dhcp-server-config \
    openssl-conf \
    kernel-image \
"
