# --*- bitbake -*--
LICESNSE = "CLOSED"
IMAGE_FSTYPES = "ext4.xz tar"

WKS_FILES:sun7i = "bpir.wks"
WKS_FILES:aml-meson = "amlogic.wks"

FEATURE_PACKAGES_nfs-client += "gssproxy"

FEATURE_PACKAGES_ns-server += "\
    bind \
    bind-setup \
    bind-utils \
"

FEATURE_PACKAGES_krb5-server += "\
    krb5-admin-server \
    krb5-kdc \
    krb5-kpropd \
    krb5-otp \
    krb5-user \
"

FEATURE_PACKAGES_ldap-server += "\
    tinyldap \
    tinyldap-utils \
"

FEATURE_PACKAGES_dhcp-server += "\
    dhcp-server \
    dhcp-setup \
"

FEATURE_PACKAGES_asterisk += "\
    asterisk \
    asterisk-sounds-de \
"

FEATURE_PACKAGES_pam-krb5 += "\
    pam-krb5 \
"

FEATURE_PACKAGES_nss-ldap += "\
    nss-pam-ldapd-nss \
"

DEPENDS += "\
    virtual/dtb \
    u-boot-bpi \
    elito-devicetree \
"

IMAGE_BOOT_FILES:bananapi-m2b = "\
    zImage;kernel-bpir-m2b.img.0 \
    zImage;kernel-bpir-m2b.img.1 \
    ${MACHINE}.dtb;oftree-bpir-m2b.dtb.0 \
    ${MACHINE}.dtb;oftree-bpir-m2b.dtb.1 \
"

IMAGE_BOOT_FILES:bananapi-m5 = "\
    Image;kernel-bpir-m5.img.0 \
    Image;kernel-bpir-m5.img.1 \
    ${MACHINE}.dtb;oftree-bpir-m5.dtb.0 \
    ${MACHINE}.dtb;oftree-bpir-m5.dtb.1 \
"

FEATURE_PACKAGES_extra-diagnostics = "\
    packagegroup-bpi-debug \
"

IMAGE_INSTALL += "\
    packagegroup-bpi-core \
    ${@bb.utils.contains('IMAGE_FEATURES', 'debug-tweaks', 'packagegroup-bpi-debug', '', d)} \
"

inherit elito-image
inherit elito-nfs-export

ROOTFS_POSTPROCESS_COMMAND += "bpi_fixup_rootfs; bpi_setup_misc;"

SED_NSS_LDAP_ENABLE = "\
    -e 's!^\(passwd\|group\|shadow\):[[:space:]]\+.*!\0 ldap!' \
"

bpi_setup_misc() {
    if ${@bb.utils.contains('IMAGE_FEATURES', 'nss-ldap', 'true', 'false', d)}; then
         sed -i ${SED_NSS_LDAP_ENABLE} ${IMAGE_ROOTFS}/${sysconfdir}/nsswitch.conf
    fi
}

bpi_fixup_rootfs() {
    if [ -e ${IMAGE_ROOTFS}${sbindir}/dropbear ] ; then
        rm -f ${IMAGE_ROOTFS}/etc/default/dropbear
        cat << EOF > ${IMAGE_ROOTFS}/etc/default/dropbear
DROPBEAR_EXTRA_ARGS=-s
DROPBEAR_RSAKEY_DIR=/persist/dropbear
EOF
    fi

    echo "${hostname}" > ${IMAGE_ROOTFS}/etc/hostname
}
