IMAGE_FEATURES += "read-only-rootfs devel-sshkey"

inherit core-image
include bpi-image-common.inc

IMAGE_FEATURES += "ssh-server-openssh"

IMAGE_INSTALL += "\
  tcpdump \
  strace \
  iproute2 \
  ppp-oe \
  ppp \
  ethtool \
"
