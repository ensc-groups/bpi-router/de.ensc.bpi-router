inherit packagegroup

RDEPENDS:${PN} = "\
    kernel-modules \
    less \
    bash \
    coreutils \
    procps \
    tzdata \
"
