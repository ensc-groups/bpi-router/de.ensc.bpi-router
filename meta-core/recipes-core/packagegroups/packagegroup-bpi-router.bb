inherit packagegroup

RDEPENDS:${PN} = "\
    bpi-router-setup-local \
    bpi-router-setup \
    chrony \
    chronyc \
    conntrack-tools-conntrackctl \
    dhcpd-pd \
    ethtool \
    iftop \
    iproute2 \
    iproute2-ifstat \
    iproute2-lnstat \
    iproute2-nstat \
    iproute2-rtacct \
    iproute2-ss \
    iproute2-tc \
    ipset \
    iptables \
    iptables-modules \
    nftables \
    iputils \
    nftables \
    nmap \
    openvpn \
    phytool \
    ppp \
    ppp-oe \
    radvd \
    rng-tools \
    rrdtool \
    net-snmp-client \
    tcpdump \
    traceroute \
    wireguard-tools \
    iftraf \
    solar-stats \
    cam-logger \
    cam-logger-socket \
    \
    bpir-policy-policy \
    auditd \
    \
    apcupsd \
    \
    systemd-journal-upload \
"
