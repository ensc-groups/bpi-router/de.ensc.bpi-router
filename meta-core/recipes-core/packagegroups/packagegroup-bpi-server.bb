inherit packagegroup

RDEPENDS:${PN} = "\
    bpi-server-setup-local \
    bpi-server-setup \
    bind-utils \
    chrony \
    chronyc \
    emacs-full \
    ethtool \
    git \
    iftop \
    iproute2 \
    iproute2-ifstat \
    iproute2-lnstat \
    iproute2-nstat \
    iproute2-rtacct \
    iproute2-ss \
    iproute2-tc \
    ipset \
    iptables \
    iptables-modules \
    nftables \
    iputils \
    nftables \
    nmap \
    phytool \
    rng-tools \
    rrdtool \
    net-snmp-client \
    \
    bpir-policy-policy \
    auditd \
    \
    systemd-journal-upload \
"
