inherit packagegroup

RRECOMMENDS:${PN} = "\
    devmem2 \
    ethtool \
    findutils \
    gdbserver \
    i2c-tools \
    iftop \
    iperf3 \
    ldd \
    lsof \
    openssl-bin \
    openssl-engines \
    procps \
    rsync \
    strace \
    stress \
    systemd-analyze \
    tcpdump \
    \
    selinux-python-audit2allow \
    ${@bb.utils.contains('DISTRO_FEATURES', 'usbhost', 'usbutils', '', d)} \
"
