LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"

PV = "3.14.14"

SRC_URI = "\
    https://sourceforge.net/projects/apcupsd/files/apcupsd%20-%20Stable/${PV}/apcupsd-${PV}.tar.gz \
    file://no-man.patch \
"

SRC_URI[sha256sum] = "db7748559b6b4c3784f9856561ef6ac6199ef7bd019b3edcd7e0a647bf8f9867"

#EXTRA_OECONF = "--help"

EXTRA_OECONF += "\
    --with-cgi-bin='${libexecdir}/apcupsd/cgi' \
    --with-distname=debian \
    sysconfdir=${sysconfdir}/apcupsd \
    SCRIPTSHELL=/bin/sh \
    WALL=${bindir}/wall \
"

EXTRA_OEMAKE = "\
    STRIP= \
    MAN=true \
    COL=true \
    V= \
"

inherit autotools-brokensep pkgconfig

do_install:append() {
    rm -rf ${D}${sysconfdir}/init.d
}

PACKAGECONFIG ??= "cgi apcsmart dumb usb dumb"

PACKAGECONFIG[cgi] = "--enable-cgi,--disable-cgi,gd"
PACKAGECONFIG[apcsmart] = "--enable-apcsmart,--disable-apcsmart"
PACKAGECONFIG[dumb] = "--enable-dumb,--disable-dumb"
PACKAGECONFIG[usb] = "--enable-usb,--disable-usb"
PACKAGECONFIG[snmp] = "--enable-snmp,--disable-snmp"
PACKAGECONFIG[pcnet] = "--enable-pcnet,--disable-pcnet"
PACKAGECONFIG[modbus-usb] = "--enable-modbus-usb,--disable-modbus-usb"
PACKAGECONFIG[gapcmon] = "--enable-gapcmon,--disable-gapcmon"

PACKAGE_BEFORE_PN += "${PN}-extra ${PN}-cgi ${PN}-conf "

RRECOMMENDS:${PN} += "\
    virtual/apcupsd-conf \
"

FILES:${PN}-extra = "\
    ${datadir}/hal \
"

RPROVIDES:${PN}-conf += "virtual/apcupsd-conf"
FILES:${PN}-conf = "\
    ${sysconfdir}/apcupsd/*.conf \
"

FILES:${PN}-cgi = "\
    ${libexecdir}/apcupsd/cgi \
    ${sysconfdir}/apcupsd/*.css \
"
