FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "\
    file://CA-bigo.ensc.de.crt \
"

## use :prepend() because main do_install() creates index with available
## certs and would miss new ones with :append()
do_install:prepend() {
	install -d -m 0755 ${D}${datadir}/ca-certificates/local
	install -p -m 0644 ${WORKDIR}/CA-bigo.ensc.de.crt ${D}${datadir}/ca-certificates/local/
}
