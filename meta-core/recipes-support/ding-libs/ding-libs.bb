LICENSE = "GPL-3.0-or-later & LGPL-3.0-or-later"
LIC_FILES_CHKSUM = "\
    file://COPYING;md5=d32239bcb673463ab874e80d47fae504 \
    file://COPYING.LESSER;md5=0ce682cee9e5b35c71c890b0458423f1 \
"

SRC_URI = "\
    git+https://github.com/SSSD/ding-libs.git;branch=master \
"

PV = "0.6.2"
SRCREV = "e5b5b1d44f476cfe406a86ec4600a13796361851"

S = "${WORKDIR}/git"

inherit autotools pkgconfig gettext

do_configure:prepend() {
    # | configure.ac:57: error: required file 'build/config.rpath' not found
    mkdir -p ${S}/build
    touch ${S}/build/config.rpath
}

python populate_packages:prepend () {
    libdir = d.expand("${libdir}")
    pnbase = d.expand("${PN}-lib%s")
    do_split_packages(d, libdir, '^lib(.*)\.so\..*', pnbase, '${BPN} %s library',
                      prepend=True, extra_depends = '', allow_links=True)
}
