LICENSE = "GPL-2 && GPL-3"
LIC_FILES_CHKSUM = "file://COPYING;md5=8297c1976c8ea23fd2abd9ffb8bd40a2"

SRC_URI = "\
    git+https://github.com/networkupstools/nut.git;branch=master \
    file://autogen-only.patch \
"

PR = "2.8.2"
SRCREV = "440ca2348e665abf3787c30bdcd9373b479f4efc"

S = "${WORKDIR}/git"

inherit autotools-brokensep python3targetconfig pkgconfig perlnative systemd

EXTRA_OECONF="\
    --with-group=nut \
    --with-user=nut \
    --with-pidpath=/run \
"

do_autogen[dirs] = "${S}"
do_autogen() {
    sh ./autogen.sh
}

do_configure[prefuncs] += "do_autogen"

PACKAGECONFIG_DEFAULT = "\
    libusb serial net-snmp neon modbus \
    openssl tcpwrap systemd \
    ${@bb.utils.contains('DISTRO_FEATURES', 'zeroconf', 'avahi', '', d)} \
"

PACKAGECONFIG ??= "${PACKAGECONFIG_DEFAULT}"

PACKAGECONFIG[libusb] = "--with-usb,--without-usb,libusb1"
PACKAGECONFIG[net-snmp] = "--with-snmp,--without-snmp,net-snmp"
PACKAGECONFIG[neon] = "--with-neon,--without-neon --with-neon-includes= --with-neon-libs=,neon"
PACKAGECONFIG[powerman] = "--with-powerman,--without-powerman,llnc-libpowerman"
PACKAGECONFIG[modbus] = "--with-modbus,--without-modbus --with-modbus-includes= --with-modbus=libs=,libmodbus"
PACKAGECONFIG[gpio] = "--with-gpio,--without-gpio --with-gpio-includes= --with-gpio-libs=,libgpio"
PACKAGECONFIG[avahi] = "--with-avahi,--without-avahi --with-avahi-includes= --with-avahi-libs=,avahi"
PACKAGECONFIG[ipmi] = "--with-ipmi,--without-ipmi,libipmi"
PACKAGECONFIG[freeipmi] = "--with-freeipmi,--without-freeipmi,libipmi"
PACKAGECONFIG[i2c] = "--with-linux_i2c,--without-linux_i2c,libi2c"
PACKAGECONFIG[openssl] = "--with-openssl,--without-openssl,openssl"
PACKAGECONFIG[nss] = "--with-nss,--without-nss,nss"
PACKAGECONFIG[tcpwrap] = "--with-wrap,--without-wrap,tcp-wrappers"
PACKAGECONFIG[libltdl] = "--with-libltdl,--without-libltdl --with-libltdl-includes= --with-libltdl-libs="
PACKAGECONFIG[systemd] = ",--without-systemdsystemunitdir --without-systemdshutdowndir --without-systemdtmpfilesdir,systemd"

PACKAGECONFIG[serial] = "--with-serial,--without-serial"
PACKAGECONFIG[cgi] = "--with-cgi,--without-cgi"
