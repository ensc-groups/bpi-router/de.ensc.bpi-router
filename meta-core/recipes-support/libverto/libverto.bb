LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=bc8917ab981cfa6161dc29319a4038d9"

SRC_URI = "\
    git+https://github.com/latchset/libverto.git;nobranch=1;branch=${PV} \
"

PV = "0.3.2"
SRCREV = "1e8f574f702f270d96e42348e7cc088da5acc9ae"

S = "${WORKDIR}/git"

inherit autotools pkgconfig

## HACK: build of final image fails else with
## | Missing or unbuildable dependency chain was: ['server-image', 'buildtools-tarball',  ...
DEPENDS:append:class-target = " libev"
#DEPENDS += "libev"

BBCLASSEXTEND = "native nativesdk"
