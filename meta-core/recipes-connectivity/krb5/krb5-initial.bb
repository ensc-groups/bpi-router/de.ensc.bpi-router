### Helper package to build libtirpc with gssapi support.  Using
### normal 'krb5' causes a dependency loop; prevent it by packaging
### only the needed libraries and headers.

FILESEXTRAPATHS:append = ":${LAYERDIR_org.openembedded.meta_meta-oe}/recipes-connectivity/krb5/krb5"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${S}/../NOTICE;md5=3c7414a99de5452b8f809ae2753b0855"

inherit autotools-brokensep
inherit nopackages
inherit features_check

REQUIRED_DISTRO_FEATURES = "rpcgss"

PV = "1.20.2"

SHRT_VER = "${@oe.utils.trim_version("${PV}", 2)}"
SRC_URI = "\
    http://web.mit.edu/kerberos/dist/krb5/${SHRT_VER}/${BP}.tar.gz \
    file://debian-suppress-usr-lib-in-krb5-config.patch;striplevel=2 \
    file://crosscompile_nm.patch \
    file://libcomerr-version.patch \
"

BP = "krb5-${PV}"

SRC_URI[md5sum] = "7ac456e97c4959ebe5c836dc2f5aab2c"
SRC_URI[sha256sum] = "7d8d687d42aed350c2525cb69a4fc3aa791694da6761dccc1c42c2ee7796b5dd"

S = "${WORKDIR}/${BP}/src"

DEPENDS = "bison-native"

PACKAGECONFIG ??= ""
PACKAGECONFIG[libedit] = "--with-libedit,--without-libedit,libedit"
PACKAGECONFIG[openssl] = "--with-crypto-impl=openssl,,openssl"
PACKAGECONFIG[keyutils] = "--with-keyutils,--without-keyutils,keyutils"
PACKAGECONFIG[ldap] = "--with-ldap,--without-ldap,openldap"
PACKAGECONFIG[readline] = "--with-readline,--without-readline,readline"
PACKAGECONFIG[pkinit] = "--enable-pkinit, --disable-pkinit"

PACKAGECONFIG[system-verto] = "--with-system-verto,--without-system-verto,libverto"
PACKAGECONFIG[system-et] = "--with-system-et,--without-system-et,e2fsprogs"

EXTRA_OECONF += "--disable-rpath"
CACHED_CONFIGUREVARS += "krb5_cv_attr_constructor_destructor=yes ac_cv_func_regcomp=yes \
                  ac_cv_printf_positional=yes ac_cv_file__etc_environment=yes \
                  ac_cv_file__etc_TIMEZONE=no"

CFLAGS:append = " -fPIC -DDESTRUCTOR_ATTR_WORKS=1 -I${STAGING_INCDIR}/et"
CFLAGS:append:riscv64 = " -D_REENTRANT -pthread"

LDFLAGS:append = " -pthread"

prefix = "/opt/krb5"
LTO = ""

do_configure() {
    gnu-configize --force
    autoreconf
    oe_runconf
}

do_install:append() {
    sed -e 's@[^ ]*-ffile-prefix-map=[^ "]*@@g' \
        -e 's@[^ ]*-fdebug-prefix-map=[^ "]*@@g' \
        -e 's@[^ ]*-fmacro-prefix-map=[^ "]*@@g' \
        -i ${D}${bindir}/krb5-config

    for i in ${D}${bindir}/*; do
        case $i in
            */krb5-config) ;;
            *)    rm -f "$i" ;;
	esac
    done

    rm -rf ${D}/run ${D}/var
    rm -rf ${D}${libdir}/krb5
    rm -rf ${D}${sbindir}
    rm -rf ${D}${datadir}
}

PACKAGES = ""
SYSROOT_DIRS += "${bindir}"
EXCLUDE_FROM_SHLIBS = "1"
