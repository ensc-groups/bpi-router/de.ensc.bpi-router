PACKAGECONFIG:append = " system-verto"
PACKAGECONFIG:append = " readline openssl keyutils"

PACKAGECONFIG[system-verto] = "--with-system-verto,--without-system-verto,libverto"

PACKAGES:remove = "${@bb.utils.contains('PACKAGECONFIG', 'system-verto', 'libverto', '', d)}"

python() {
    pkgs = d.getVar('PACKAGES', False).split()
    try:
        pkgs.remove('${PN}-user')
        ## HACK: must be done here too
        pkgs.remove(bb.utils.contains('PACKAGECONFIG', 'system-verto', 'libverto', '', d))
        d.setVar('PACKAGES', ' '.join(pkgs))
    except:
        pass
}
