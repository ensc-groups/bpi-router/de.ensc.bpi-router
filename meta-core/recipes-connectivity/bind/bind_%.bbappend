FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "\
    file://bind.tmpfiles \
"

do_install:append() {
	install -D -p -m 0644 ${WORKDIR}/bind.tmpfiles \
		${D}${libdir}/tmpfiles.d/bind.conf

	rm -rf ${D}${sysconfdir}/tmpfiles.d
}

FILES:${PN} += "${libdir}/tmpfiles.d/bind.conf"
