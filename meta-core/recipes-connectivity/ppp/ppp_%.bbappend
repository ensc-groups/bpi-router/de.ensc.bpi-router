FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

## From Fedora ppp package
SRC_URI += "\
  file://0012-pppd-we-don-t-want-to-accidentally-leak-fds.patch \
  file://0013-everywhere-O_CLOEXEC-harder.patch \
  file://0014-everywhere-use-SOCK_CLOEXEC-when-creating-socket.patch \
"

inherit pkgconfig

PACKAGECONFIG ??= "systemd"

PACKAGECONFIG[systemd] = "--enable-systemd,--disable-systemd,systemd"
