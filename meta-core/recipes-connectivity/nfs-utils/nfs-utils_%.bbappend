FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "\
    file://cross-compile.patch \
"

DEPENDS += "lvm2"

PACKAGECONFIG:append = "${@bb.utils.contains('DISTRO_FEATURES', 'nfs4', ' nfsv4 nfsv41', '', d)}"
PACKAGECONFIG:append = "${@bb.utils.contains('DISTRO_FEATURES', 'rpcgss', ' gss', '', d)}"
PACKAGECONFIG:append = "${@bb.utils.contains('DISTRO_FEATURES', 'systemd', ' systemd', '', d)}"

PACKAGECONFIG[gss] = "--enable-gss --with-krb5='${STAGING_EXECPREFIXDIR}',--disable-gss,krb5"
PACKAGECONFIG[systemd] = "--with-systemd=${systemd_system_unitdir},--without-systemd"

PACKAGE_BEFORE_PN += "${PN}-gss ${PN}-idmapd ${PN}-nfsidmap ${PN}-common ${PN}-server"

####

RRECOMMENDS:remove:${PN} = "kernel-module-nfsd"

# common

FILES:${PN}-common += "\
    ${sysconfdir}/nfs-utils.conf \
    ${sysconfdir}/init.d/nfscommon \
    ${nonarch_base_libdir}/udev/rules.d/* \
    ${sbindir}/rpc.gssd \
    ${systemd_system_unitdir}/auth-rpcgss-module.service \
    ${systemd_system_unitdir}/nfs-utils.service \
    ${systemd_system_unitdir}/rpc-gssd.service \
    ${systemd_system_unitdir}/rpc_pipefs.target \
    ${systemd_system_unitdir}/var-lib-nfs-rpc_pipefs.mount \
    ${systemd_unitdir}/system-generators/rpc-pipefs-generator \
"

SYSTEMD_SERVICE:${PN} = ""

# nfsidmap

FILES:${PN}-nfsidmap += "\
    ${sysconfdir}/request-key.d/id_resolver.conf \
    ${sbindir}/nfsidmap \
    ${libdir}/libnfsidmap* \
"

# idmapd

FILES:${PN}-idmapd += "\
    ${sbindir}/rpc.idmapd \
    ${systemd_system_unitdir}/nfs-idmapd.service \
    ${systemd_system_unitdir}/rpc-idmapd.service \
"

# client

FILES:${PN}-client:remove = "\
    ${sysconfdir}/init.d/nfscommon \
    ${sysconfdir}/nfs-utils.conf \
    ${sbindir}/rpc.idmapd \
    ${libdir}/libnfsidmap.so.* \
"

FILES:${PN}-client += "\
    ${sysconfdir}/nfsmount.conf \
    ${sbindir}/blkmapd \
    ${systemd_system_unitdir}/nfs-blkmap.service \
    ${systemd_system_unitdir}/rpc-statd-notify.service \
    ${systemd_system_unitdir}/rpc-statd.service \
"
RDEPENDS:${PN}-client += "${PN}-common ${PN}-nfsidmap"

SYSTEMD_SERVICE:${PN}-client = "nfs-client.target"
RRECOMMENDS:${PN}-client += "\
    kernel-module-auth-rpcgss \
    kernel-module-rpcsec-gss-krb5 \
    kernel-module-sunrpc \
"

#server

FILES:${PN}-server += "\
    ${sbindir}/exportfs \
    ${sbindir}/fsidd \
    ${sbindir}/rpc.mountd \
    ${sbindir}/rpc.nfsd \
    ${systemd_system_unitdir}/fsidd.service \
    ${systemd_system_unitdir}/nfs-mountd.service \
    ${systemd_system_unitdir}/nfs-server.service \
    ${systemd_system_unitdir}/proc-fs-nfsd.mount \
    ${systemd_unitdir}/system-generators/nfs-server-generator \
"
RDEPENDS:${PN}-server += "${PN}-common ${PN}-idmapd"
RRECOMMENDS:${PN}-server += "\
    kernel-module-nfsd \
    kernel-module-rpcsec-gss-krb5 \
"

SYSTEMD_SERVICE:${PN}-server = "nfs-server.service"


do_install_local() {
	autotools_do_install

	install -d -m 0755 ${D}${sysconfdir}
	install    -p -m 0644 ${WORKDIR}/nfs-utils.conf ${D}${sysconfdir}/
	install    -p -m 0644 ${S}/utils/mount/nfsmount.conf ${D}${sysconfdir}/
	install -D -p -m 0644 ${S}/utils/nfsidmap/id_resolver.conf ${D}${sysconfdir}/request-key.d/id_resolver.conf
}

python() {
    d.setVar('do_install', 'do_install_local')
}
