FILESEXTRAPATHS:prepend := "${THISDIR}/openssh:"

RO_SSHD_PATH = "/persist/ssh"

PACKAGECONFIG:append = " kerberos"

RO_SED_CMD = "\
    -e 's:^#\?[[:space:]]*\(HostKey[[:space:]]\+\).*\(/[^/]\+\):\1${RO_SSHD_PATH}\2:' \
"

do_install:append () {
	## generic, upstreamable change
	if ! [ "${@bb.utils.filter('DISTRO_FEATURES', 'pam', d)}" ]; then
		sed -i -e 's:^UsePAM yes:UsePAM no:' ${D}${sysconfdir}/ssh/sshd_config*
	fi
}

do_install:append () {
	sed ${RO_SED_CMD} ${D}${sysconfdir}/ssh/sshd_config > \
	     ${D}${sysconfdir}/ssh/sshd_config_readonly
}
