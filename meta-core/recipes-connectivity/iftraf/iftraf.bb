LICENSE = "GPL-3.0-or-later"
LIC_FILES_CHKSUM="file://COPYING;md5=1ebbd3e34237af26da5dc08a4e440464"
SUMMARY = "traffic monitoring"
HOMEPAGE = "https://gitlab.com/ensc-groups/bpi-router/tools/iftraf"
SECTION = "net"

SRC_URI = "\
    ${URI_PROJECT_TOOLS}/iftraf;branch=master \
    ${CRATES} \
"
SRCREV = "0205c420c910f44e688d31cd461d602bc04d1993"
S = "${WORKDIR}/git"
B = "${S}"

require iftraf-crates.inc

inherit cargo systemd cargo-fixup-frozen useradd

DEPENDS += "rrdtool"

do_install:append() {
    mv ${D}${bindir} ${D}${sbindir}
    install -D -p -m 0644 ${S}/contrib/iftraf.service ${D}${systemd_system_unitdir}/${PN}.service
}

SYSTEMD_SERVICE:${PN} = "${PN}.service"

USERADD_PACKAGES += "${PN}"

GROUPADD_PARAM:${PN} = "--system iftraf"
USERADD_PARAM:${PN} = "--system --home / -M -g iftraf iftraf"
