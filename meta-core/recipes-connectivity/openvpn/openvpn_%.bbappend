FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

inherit pkgconfig

FILES:${PN} += "${libdir}/tmpfiles.d"

PACKAGECONFIG ??= "\
    ${@bb.utils.filter('DISTRO_FEATURES', 'systemd', d)} \
    ${@bb.utils.filter('DISTRO_FEATURES', 'selinux', d)} \
    lz4 \
"

PACKAGECONFIG[systemd]  = "--enable-systemd,--disable-systemd,systemd"
PACKAGECONFIG[selinux]  = "--enable-selinux,--disable-selinux"
PACKAGECONFIG[lz4]      = "--enable-lz4,--disable-lz4,lz4"

EXTRA_OECONF += "\
    --enable-x509-alt-username \
    SYSTEMD_UNIT_DIR='${systemd_system_unitdir}' \
"

FILES:${PN} += "${systemd_system_unitdir}/openvpn-*@.service"
FILES:${PN}-sample += "${systemd_unitdir}/system/openvpn@.service"
SYSTEMD_SERVICE:${PN} = ""
SYSTEMD_SERVICE:${PN}-sample = "openvpn@loopback-server.service openvpn@loopback-client.service"

inherit useradd

USERADD_PACKAGES += "${PN}"

GROUPADD_PARAM:${PN} = "--system openvpn"
USERADD_PARAM:${PN} = "--system --home / -M -g openvpn openvpn"
