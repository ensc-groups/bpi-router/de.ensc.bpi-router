LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=a9ac3d0a983ebc781f7aa7173499e2e5"

SRC_URI = "\
    git+https://github.com/gssapi/gssproxy.git;branch=main \
    file://cross-compile.patch \
    file://keytab-paths.patch \
"

PV = "0.9.2"
SRCREV = "27f883ff3a75dc7b52d0b0bd692f9eeb8ee53338"

S = "${WORKDIR}/git"

inherit autotools pkgconfig systemd gettext

EXTRA_OECONF += "\
    --without-manpages \
    xVERTO_CFLAGS=' ' \
    xVERTO_LIBS='-lverto' \
"

DEPENDS += "krb5 keyutils popt ding-libs systemd libverto"

PACKAGECONFIG ??= "cap hardening selinux"

PACKAGECONFIG[cap] = "--with-cap,--without-cap,libcap"
PACKAGECONFIG[hardening] = "--with-hardening,--without-hardeining"
PACKAGECONFIG[selinux] = "--with-selinux,--without-selinux"

do_install:append() {
    install -D -p -m 644 examples/gssproxy.conf              ${D}/${sysconfdir}/gssproxy/gssproxy.conf
    install -D -p -m 644 examples/24-nfs-server.conf         ${D}/${sysconfdir}/gssproxy/24-nfs-server.conf
    install -D -p -m 644 examples/99-network-fs-clients.conf ${D}/${sysconfdir}/gssproxy/99-network-fs-clients.conf

    install -D -p -m 644 examples/proxymech.conf             ${D}/${sysconfdir}/gss/mech.d/proxymech.conf

    rm -rf ${D}/var/log
}

SYSTEMD_SERVICE:${PN} = "gssproxy.service"

FILES:${PN} += "${systemd_user_unitdir}/*"
