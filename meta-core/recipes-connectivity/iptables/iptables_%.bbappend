PACKAGECONFIG:append = " libnftnl libnfnetlink"

do_install:append() {
	cd ${D}${sbindir}
	rm -f ${_ALTERNATIVE}
}

PACKAGES =+ "${PN}-lib ${PN}-nft ${PN}-legacy"

ALLOW_EMPTY:${PN}-nft    = "1"
ALLOW_EMPTY:${PN}-legacy = "1"

#RDEPENDS:${PN}-nft    = "${PN}"
#RDEPENDS:${PN}-legacy = "${PN}"

RPROVIDES:${PN}-nft    += "virtual/iptables-bin"
RPROVIDES:${PN}-legacy += "virtual/iptables-bin"

RDEPENDS:${PN} += "virtual/iptables-bin"

_ALTERNATIVE = "\
    ip6tables ip6tables-restore ip6tables-save \
    iptables  iptables-restore  iptables-save \
"

ALTERNATIVE_PRIORITY:${BPN}-nft    = "20"
ALTERNATIVE_PRIORITY:${BPN}-legacy = "10"

FILES:${PN}-legacy = "\
    ${bindir}/iptables-xml \
    ${sbindir}/*-legacy \
    ${sbindir}/*-legacy-* \
    ${sbindir}/xtables-legacy-multi \
"

FILES:${PN}-nft    = "\
    ${sbindir}/*-nft \
    ${sbindir}/*-nft-* \
    ${sbindir}/arptables* \
    ${sbindir}/ebtables* \
    ${sbindir}/*-translate \
    ${sbindir}/xtables-monitor \
    ${sbindir}/xtables-nft-multi \
"

FILES:${PN}-lib = "${libdir}/*.so.*"

python() {
    pn  = d.getVar('PN', True)
    alt = d.getVar('_ALTERNATIVE', True).split() or ()
    d.setVar('ALTERNATIVE:%s-nft'    % pn, ' '.join(alt))
    d.setVar('ALTERNATIVE:%s-legacy' % pn, ' '.join(alt))

    for a in alt:
        d.setVarFlag('ALTERNATIVE_LINK_NAME', a, '${sbindir}/%s' %a)
        d.setVarFlag('ALTERNATIVE_TARGET_%s-nft'    % pn, a, '${sbindir}/xtables-nft-multi')
        d.setVarFlag('ALTERNATIVE_TARGET_%s-legacy' % pn, a, '${sbindir}/xtables-legacy-multi')
}

## check_data_file_clashes: Package iptables wants to install file .../rootfs/etc/ethertypes
##        But that file is already provided by package  * netbase
do_install:append() {
    rm ${D}${sysconfdir}/ethertypes
}

inherit update-alternatives
