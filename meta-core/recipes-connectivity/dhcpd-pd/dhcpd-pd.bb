LICENSE = "GPL-3.0-or-later"
LIC_FILES_CHKSUM="file://COPYING;md5=1ebbd3e34237af26da5dc08a4e440464"
SUMMARY = "DHCPv6 client implementing prefix delegation (RFC 3633)"
HOMEPAGE = "https://gitlab.com/ensc-groups/bpi-router/tools/dhcpd-pd"
SECTION = "net"

SRC_URI = "\
    ${URI_PROJECT_TOOLS}/dhcpd-pd;branch=master \
"
SRCREV = "0205c420c910f44e688d31cd461d602bc04d1993"
S = "${WORKDIR}/git"

inherit autotools systemd

EXTRA_OEMAKE = "\
  -f ${S}/Makefile \
  CC='${CC}' \
  CFLAGS='${CFLAGS}' \
  LDFLAGS='${LDFLAGS}' \
  prefix=${prefix} \
  sbindir=${sbindir} \
"

#EXTRA_OEMAKE += "DEBUG_LEVEL=0xffff"

DEPENDS += "openssl"

do_install:append() {
	install -D -p -m 0644 ${S}/contrib/dhcpd-pd@.service ${D}${systemd_system_unitdir}/dhcpd-pd@.service
}

FILES:${PN} += "${systemd_system_unitdir}/dhcpd-pd@.service"
