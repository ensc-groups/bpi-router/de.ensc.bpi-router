sign_modules[dirs] = "${B}"
sign_modules() {
  grep -q '^# CONFIG_MODULE_SIG_ALL is not set' .config || return 0
  grep -q '^CONFIG_MODULE_SIG=y' .config || return 0

  eval "$( grep ^CONFIG_MODULE_SIG_HASH= .config )"
  eval "$( grep ^CONFIG_MODULE_SIG_KEY= .config )"

  find '${PKGDEST}'/*-module-* -type f -name '*.ko' | \
    xargs -tn1 ${B}/scripts/sign-file "$CONFIG_MODULE_SIG_HASH" "$CONFIG_MODULE_SIG_KEY" certs/signing_key.x509
}

PACKAGEFUNCS =+ "sign_modules"
