SE_POLICY_NAME ?= "minimum"
SE_POLICY_NAME[doc] = "The name of the policy; commonly supported names are 'minimum', 'targeted', 'standard' or 'mls'"

SE_REFERENCE_POLICY ?= "refpolicy-${SE_POLICY_NAME}"
SE_REFERENCE_POLICY[doc] = "The reference policy recipe"

SE_EXTRA_POLICY_MODULES ?= ""
SE_EXTRA_POLICY_MODULES[doc] = "Policy modules from the reference policy which shall be installed additionally"
SE_EXTRA_POLICY_MODULES[type] = "list"

SE_ENFORCING ?= ""
SE_ENFORCING[doc] = "When set, the SELinux state will be modified to the given value; accepted values are 'permissive', 'enforcing' and 'disabled'"

# we need the modules and local store from the reference policy
DEPENDS +=  "${SE_REFERENCE_POLICY}"
DEPENDS += "bzip2-replacement-native checkpolicy-native policycoreutils-native semodule-utils-native m4-native"

# flags for building local modules
SE_OEMAKE = "\
  -f '${SE_HEADERDIR}/Makefile' \
  NAME='${SE_POLICY_NAME}' \
  SHAREDIR=${SE_SHAREDIR} \
  HEADERDIR=.selinux/inc \
  BINDIR='${STAGING_BINDIR_NATIVE}' \
  SBINDIR='${STAGING_SBINDIR_NATIVE}' \
"

SE_SHAREDIR = "${STAGING_DATADIR}/selinux"
SE_HEADERDIR = "${SE_SHAREDIR}/${SE_POLICY_NAME}/include"

EXTRA_OEMAKE += "${SE_OEMAKE}"

# create and prettify list of core modules with their full path
_SE_EXTRA_POLICY_MODULES_PP = "\
  ${@' '.join(sorted(set(map(lambda x: '${STAGING_DATADIR}/selinux/${SE_POLICY_NAME}/%s.pp' % x, \
    d.getVar('SE_EXTRA_POLICY_MODULES', True).split()))))} \
    "

SE_POLICY_ROOT = "${WORKDIR}/policy-root"
SE_POLICY_ROOT[doc] = "Temporary workspace where 'semanage' will operate"

# use '=+' to construct package list to avoid putting too much data into ${PN}
PACKAGES =+ "${PN}-policy ${PN}-extra ${PN}-modules"

# system can work without these files, but many tools require them to
# e.g. create a correct file context
FILES:${PN}        += "\
  ${sysconfdir}/selinux \
"

# the core policy and configuration file
FILES:${PN}-policy += "\
  ${sysconfdir}/selinux/${SE_POLICY_NAME}/policy \
  ${sysconfdir}/selinux/config \
"
CONFFILES:${PN}-policy += "${sysconfdir}/selinux/config"

FILES:${PN}-extra += "\
  ${localstatedir}/lib/selinux \
"

FILES:${PN}-modules += "\
  ${datadir}/selinux/${SE_POLICY_NAME}/*.pp \
"

RRECOMMENDS:${PN}-policy = "${PN}"

BUILDVARS_EXPORT += "\
  SE_OEMAKE \
  SE_POLICY_ROOT \
  SE_POLICY_NAME \
  SE_SHAREDIR \
  SE_HEADERDIR \
  SE_LOCAL_POLICY_MODULES \
"

se_semodule() {
	## add -D for dontaudit logging
	semodule -v -p '${SE_POLICY_ROOT}' -s '${SE_POLICY_NAME}' -n "$@"
}

se_do_configure() {
        ## HACK: create symlink to the actual header to avoid a too
        ## long cmdline which is breaking the build
	rm -rf .selinux
	mkdir -p .selinux
	ln -s ${SE_HEADERDIR} .selinux/inc
}

se_do_compile() {
	oe_runmake
}

se_do_install() {
	cd "${SE_POLICY_ROOT}"
	cfgdir=${sysconfdir}/selinux
	polcfgdir=$cfgdir/${SE_POLICY_NAME}
	statedir=${localstatedir}/lib/selinux/${SE_POLICY_NAME}
	sedatadir=${datadir}/selinux/${SE_POLICY_NAME}

	install -d -m 0755 ${D}${sedatadir}
	install -p -m 0644 ${B}/*.pp ${D}${sedatadir}/

	tar cf - --owner root --group root --mode a+rX,go-w . | tar xf - -C ${D}
	rm -f ${D}$cfgdir/semanage.conf
	rm -f ${D}$statedir/*.LOCK
	rmdir ${D}$statedir/../final
	cd -

	if test -n "${SE_ENFORCING}"; then
		sed -i -e 's!^SELINUX=.*!SELINUX=${SE_ENFORCING}!' ${D}$cfgdir/config
	fi
}

do_configure() {
	se_do_configure
}

do_compile() {
	se_do_compile
}

do_install() {
	se_do_install
}

### Prepare the workspace directory where 'semanage' can operate
do_prepare_policy[cleandirs] = "${SE_POLICY_ROOT}"
do_prepare_policy[dirs] = "${SE_POLICY_ROOT}"
do_prepare_policy() {
	install -d var/lib/selinux/ ./${sysconfdir}/selinux

	cat <<-EOF > ./${sysconfdir}/selinux/semanage.conf
module-store = direct
compiler-directory = ${STAGING_DIR_NATIVE}${libexecdir}/selinux/hll
[setfiles]
path = ${STAGING_DIR_NATIVE}${base_sbindir_native}/setfiles
args = -q -c \$@ \$<
[end]
[sefcontext_compile]
path = ${STAGING_DIR_NATIVE}${sbindir_native}/sefcontext_compile
args = \$@
[end]
EOF

	cp -a ${STAGING_DIR_TARGET}/${sysconfdir}/selinux/. ./${sysconfdir}/selinux/
	rm -rf ./${sysconfdir}/selinux/${SE_POLICY_NAME}/policy
	cp -a ${STAGING_DIR_TARGET}/var/lib/selinux/${SE_POLICY_NAME} var/lib/selinux/
}
addtask do_prepare_policy after do_prepare_recipe_sysroot

do_build_policy[dirs] = "${SE_POLICY_ROOT}"
do_build_policy() {
	# rebuild policy
	se_semodule -B

	if test -n '${_SE_EXTRA_POLICY_MODULES_PP}'; then
		# install additional core modules
		se_semodule -X 110 -i ${_SE_EXTRA_POLICY_MODULES_PP}
	fi
}
addtask do_build_policy after do_prepare_policy

_local_pp = "${@' '.join(sorted(map(lambda x: '${B}/%s.pp' % x, \
    set(d.getVar('SE_LOCAL_POLICY_MODULES', True).split()))))}"

## install the local modules
do_add_polmods() {
	# install locally built modules
	se_semodule -X 200 -i ${_local_pp}
}
addtask do_add_polmods after do_build_policy do_compile before do_install
