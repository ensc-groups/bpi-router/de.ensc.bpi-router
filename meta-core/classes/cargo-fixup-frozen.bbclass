inherit cargo

python() {
    flags = d.getVar('CARGO_BUILD_FLAGS', False).replace('--frozen', '--offline')
    d.setVar('CARGO_BUILD_FLAGS', flags)
}
