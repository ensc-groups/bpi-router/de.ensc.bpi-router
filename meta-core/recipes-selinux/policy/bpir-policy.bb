LICENSE = "CLOSED"

PACKAGE_ARCH = "${MACHINE_ARCH}"

SRC_URI = "\
    ${URI_PROJECT_TOOLS}/selinux-policy \
"
SRCREV = "7b3726a2a7f9b5a1021c910e40f6bec0dd68502f"

S = "${WORKDIR}/git"

inherit selinux-policy

SE_EXTRA_POLICY_MODULES += "\
    asterisk \
    authlogin \
    bind \
    chronyd \
    dbus \
    dhcp \
    iptables \
    gssproxy \
    kerberos \
    locallogin \
    netutils \
    nslcd \
    openvpn \
    ppp \
    radvd \
    systemd \
    ssh \
    unprivuser \
"

SE_LOCAL_POLICY_MODULES = "\
    cam-logger \
    dhcpd-pd \
    iftraf \
    tinyldap \
    solar-stats \
    local-asterisk \
    local-auth \
    local-bind \
    local-busybox \
    local-chronyd \
    local-dhcp \
    local-dhcpd-pd \
    local-iptables \
    local-kerberos \
    local-nslcd \
    local-openvpn \
    local-policy \
    local-pppd \
    local-ssh \
    local-systemd \
    local-unprivuser \
"

SE_ENFORCING = "enforcing"

do_add_polmods:append() {
	src_cfgdir=${STAGING_DIR_TARGET}/${sysconfdir}/selinux/${SE_POLICY_NAME}/contexts
	dst_cfgdir=${SE_POLICY_ROOT}/${sysconfdir}/selinux/${SE_POLICY_NAME}/contexts

	f=file_contexts.subs_dist
	{
		cat $src_cfgdir/files/$f
		cat ${S}/contexts/$f
	} > $dst_cfgdir/files/$f

	f=default_contexts
	{
		cat $src_cfgdir/$f
		cat ${S}/contexts/$f
	} > $dst_cfgdir/$f

	install -p -m 0644 ${S}/contexts/booleans \
		${SE_POLICY_ROOT}${localstatedir}/lib/selinux/${SE_POLICY_NAME}/active/booleans.local

	install -p -m 0644 ${S}/contexts/ports \
		${SE_POLICY_ROOT}${localstatedir}/lib/selinux/${SE_POLICY_NAME}/active/ports.local

	# rebuild policy
	se_semodule -B
}
