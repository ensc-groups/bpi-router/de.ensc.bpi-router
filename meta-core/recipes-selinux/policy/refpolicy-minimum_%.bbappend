#POLICY_TYPE = "mls"

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "\
    file://no-local-local.patch \
    file://local-setup.patch \
    file://user-t.patch \
    file://fixed-device.patch \
    file://policykit-optional.patch \
"

SYSROOT_DIRS += "${localstatedir}"
