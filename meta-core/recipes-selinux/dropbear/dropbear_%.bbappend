FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "\
    file://0001-added-selinux-support.patch \
"

PACKAGECONFIG[selinux] = "--enable-selinux,--disable-selinux,libselinux"

PACKAGECONFIG:append = " selinux"
