INCPATH = "${BASEPATH_de.sigma-chemnitz.core}/xtra-bsp/u-boot"

require ${INCPATH}/u-boot.inc
require ${INCPATH}/u-boot-target.inc
require ${INCPATH}/u-boot-common_2023.04.inc

PACKAGECONFIG:append = " swig"

DEPENDS += "python3-setuptools-native"

TARGET_INC = "/undefined/"
TARGET_INC:aml-meson = "meson"
TARGET_INC:sun7i     = "sun7i"

inherit python3native

require u-boot-target_${TARGET_INC}.inc
