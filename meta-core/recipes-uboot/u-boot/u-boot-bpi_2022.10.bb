INCPATH = "${BASEPATH_de.sigma-chemnitz.core}/xtra-bsp/u-boot"

require ${INCPATH}/u-boot.inc
require ${INCPATH}/u-boot-target.inc
require ${INCPATH}/u-boot-common_2022.10.inc

UBOOT_SUFFIX = "-sunxi-with-spl.bin"

PACKAGECONFIG:append = " swig"

DEPENDS += "python3-setuptools-native"

inherit python3native
