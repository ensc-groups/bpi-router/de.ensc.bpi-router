DEPENDS:append = " firmware-meson gxlimg-native"

UBOOT_SUFFIX = "-fip.bin"

FWDIR = "${STAGING_DATADIR}/firmware-meson"

FIP_ARGS = "\
    --rev v3 \
    --bl2  bl2.bin.sig \
    --bl30 bl30.bin.sig30 \
    --bl31 bl31.img.sig.broken \
    --bl33 bl33.bin.sig \
    --ddrfw '${FWDIR}'/ddr4_1d.fw \
    --ddrfw '${FWDIR}'/ddr4_2d.fw \
    --ddrfw '${FWDIR}'/ddr3_1d.fw \
    --ddrfw '${FWDIR}'/piei.fw \
    --ddrfw '${FWDIR}'/lpddr4_1d.fw \
    --ddrfw '${FWDIR}'/lpddr4_2d.fw \
    --ddrfw '${FWDIR}'/diag_lpddr4.fw \
    --ddrfw '${FWDIR}'/aml_ddr.fw \
    --ddrfw '${FWDIR}'/lpddr3_1d.fw \
"

## `gxlimg` generated images are not working; use `meson64-tools` for
## some parts
## https://github.com/repk/gxlimg/issues/19
DEPENDS:append = " meson64-tools-native"
BOOTMK_ARGS = "\
    --bl2  bl2.bin.sig \
    --bl30 bl30.bin.sig \
    --bl31 bl31.img.sig \
    --bl33 bl33.bin.sig \
    --ddrfw1 '${FWDIR}'/ddr4_1d.fw \
    --ddrfw2 '${FWDIR}'/ddr4_2d.fw \
    --ddrfw3 '${FWDIR}'/ddr3_1d.fw \
    --ddrfw4 '${FWDIR}'/piei.fw \
    --ddrfw5 '${FWDIR}'/lpddr4_1d.fw \
    --ddrfw6 '${FWDIR}'/lpddr4_2d.fw \
    --ddrfw7 '${FWDIR}'/diag_lpddr4.fw \
    --ddrfw8 '${FWDIR}'/aml_ddr.fw \
    --ddrfw9 '${FWDIR}'/lpddr3_1d.fw \
"

do_generate_bin[dirs] = "${B}/fip"
do_generate_bin[cleandirs] = "${B}/fip"
do_generate_bin() {
    PATH=$PATH:${STAGING_LIBEXECDIR_NATIVE}/meson64-tools

    rm -f ../u-boot-fip.bin

    blx-fix.sh bl30 '${FWDIR}/bl30.bin' '${FWDIR}/bl301.bin' bl30.bin
    blx-fix.sh bl2  '${FWDIR}/bl2.bin'  '${FWDIR}/acs.bin'   bl20.bin

    gxlimg -t bl2  -s bl20.bin            bl2.bin.sig
    gxlimg -t bl30 -s bl30.bin            bl30.bin.sig30
    gxlimg -t bl3x -s bl30.bin.sig30      bl30.bin.sig
    bl3sig  --input '${FWDIR}'/bl31.img --output bl31.img.sig
    gxlimg -t bl3x -s '${FWDIR}'/bl31.img bl31.img.sig.broken
    gxlimg -t bl3x -s ${B}/u-boot.bin     bl33.bin.sig

    bootmk  ${BOOTMK_ARGS}              --output u-boot-fip.bin

    gxlimg -t fip  ${FIP_ARGS}            u-boot-fip.bin.broken

    mkdir X0 X1
    gxlimg -e u-boot-fip.bin        X0/ || :
    gxlimg -e u-boot-fip.bin.broken X1/ || :

    ln -s fip/u-boot-fip.bin ..
}
addtask do_generate_bin after do_compile before do_install
