FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}-${BASE_PV}:${THISDIR}/${PN}:"

CFGSET += "\
  file://bpir.cfg \
  file://bpi-hw.cfg \
"

PATCHSET += "\
    file://0001-BPI-set-our-environment.patch \
"

COMPATIBLE_MACHINE  = "bananapi-router"
COMPATIBLE_MACHINE .= "|bananapi-m1"
COMPATIBLE_MACHINE .= "|bananapi-m2b"
COMPATIBLE_MACHINE .= "|bananapi-m5"
