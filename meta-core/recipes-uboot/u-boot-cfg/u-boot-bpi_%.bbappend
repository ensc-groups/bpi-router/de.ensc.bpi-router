FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}-${BASE_PV}:${THISDIR}/${PN}:${THISDIR}/features:"

ENVSET += "\
    file://400-generic.env \
    file://500-addr.env \
    file://500-misc.env \
    file://500-names.env \
    file://600-net.env \
"

PATCHSET += "\
    file://0001-env2string-allow-to-delete-variables.patch \
    file://no-default-env.patch \
"

CFGSET += "\
    file://bpir.cfg \
    file://bpi-hw.cfg \
"

SRC_URI += "\
    ${ENVSET} \
"

inherit uboot-envsetup

do_uboot_envsetup:append() {
    envsetup_file(d, "400-generic.env")
    envsetup_file(d, "500-addr.env")
    envsetup_file(d, "500-misc.env")
    envsetup_file(d, "500-names.env")
    envsetup_file(d, "600-net.env")
}
