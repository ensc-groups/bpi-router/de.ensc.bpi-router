FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}-${BASE_PV}:"

PATCHSET += "\
    file://0001-crypto-Add-allwinner-subdirectory.patch \
    file://0002-crypto-Add-Allwinner-sun8i-ce-Crypto-Engine.patch \
    file://0003-dt-bindings-crypto-Add-DT-bindings-documentation-for.patch \
    file://0004-ARM-dts-sun8i-a83t-Add-Security-System-node.patch \
    file://0005-ARM-dts-sun8i-r40-add-crypto-engine-node.patch \
    file://0006-ARM-dts-sun8i-h3-Add-Crypto-Engine-node.patch \
    file://0007-ARM64-dts-allwinner-sun50i-Add-Crypto-Engine-node-on.patch \
    file://0008-ARM64-dts-allwinner-sun50i-Add-crypto-engine-node-on.patch \
    file://0009-ARM64-dts-allwinner-sun50i-Add-Crypto-Engine-node-on.patch \
    file://0010-ELiTo-NFS-take-default-nfsroot-from-environment.patch \
    file://0011-flymake-initial-checkin.patch \
    file://0012-arm-sun8i-enable-ARM_ARCH_TIMER.patch \
    file://0013-sun8i-r40-enlarge-cpuif-memory-address.patch \
"

COMPATIBLE_MACHINE  = "bananapi-router"
#COMPATIBLE_MACHINE .= "|bananapi-m1"
COMPATIBLE_MACHINE .= "|bananapi-m2b"
