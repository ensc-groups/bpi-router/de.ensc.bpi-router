SELINUX_LAYER = "${SOURCES_ROOT}/org.openembedded.selinux"

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
FILESEXTRAPATHS:append   = ":${SELINUX_LAYER}/recipes-kernel/linux/files"

## remove dependency on /boot files
RRECOMMENDS:${KERNEL_PACKAGE_NAME}-base = ""

CFGSET += "\
    file://bpir-optimize.cfg \
    ${@bb.utils.contains('DISTRO_FEATURES','systemd', 'file://systemd.cfg', '', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES','selinux', 'file://selinux.cfg', '', d)} \
    file://bpir-hw.cfg \
    file://bpir-setup.cfg \
    file://bpir-net.cfg \
    file://module-sign.cfg \
"

inherit kernel-sign
