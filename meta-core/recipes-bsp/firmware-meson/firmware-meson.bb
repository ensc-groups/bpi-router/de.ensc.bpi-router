LICENSE = "CLOSED"

PACKAGE_ARCH = "${MACHINE_ARCH}"

REV = "c4bf0e3b1ab1246c3176d6c3e420a5e1cdf40c4e"
BASE_URI = "https://github.com/LibreELEC/amlogic-boot-fip/raw/${REV}"

COMPATIBLE_MACHINE = "bananapi-m5"

FW:bananapi-m5 = "\
    acs.bin \
    aml_ddr.fw \
    bl2.bin \
    bl30.bin \
    bl301.bin \
    bl31.img \
    ddr3_1d.fw \
    ddr4_1d.fw \
    ddr4_2d.fw \
    diag_lpddr4.fw \
    lpddr4_1d.fw \
    lpddr4_2d.fw \
    lpddr3_1d.fw \
    piei.fw \
"

SUBDIR:bananapi-m5 = "bananapi-m5"

SRC_URI[vardeps] += "BASE_URI SUBDIR FW"
SRC_URI = "\
    ${@dl_uri('${BASE_URI}', '${SUBDIR}', d.getVar('FW'))} \
"

DLPREFIX = "${BPN}-${SUBDIR}-"

def dl_uri(base, subdir, names):
   res = []
   for n in names.split():
       res.append("%(base)s/%(subdir)s/%(n)s;name=%(subdir)s-%(n)s;downloadfilename=${DLPREFIX}/%(n)s;subdir=_fw" % locals())

   return ' '.join(res)

BB_STRICT_CHECKSUM = "1"

SRC_URI[bananapi-m5-acs.bin.sha256sum] = "430149572853e7bddfc9276181f05696eeb318ec0441f04ce403ca08633e6ac4"
SRC_URI[bananapi-m5-aml_ddr.fw.sha256sum] = "eac3cd6d322aefddf2c4e37c4f2f2b9b3b71c02455130d6d3fbc1a99121dbfc7"
SRC_URI[bananapi-m5-bl2.bin.sha256sum] = "33e27f74ae04063b65db343511e1c70d2173bf356ee629a8fb6139f108ad063b"
SRC_URI[bananapi-m5-bl30.bin.sha256sum] = "15d8f42cc397a805dfc182c26ce08936a4047ff2c9bc754e385e3ce3b0457664"
SRC_URI[bananapi-m5-bl301.bin.sha256sum] = "b13ea78195628178276c87e051daa531b3f0f5e79021ad78bc8843d22c21e201"
SRC_URI[bananapi-m5-bl31.img.sha256sum] = "bd8098c9392b77ba65b522f4d93e14e9e9ac8bb860d68cbf71876e056ae4f1fa"
SRC_URI[bananapi-m5-ddr3_1d.fw.sha256sum] = "a12fd675a29268a185d630bb50ba47287ac4ff3f7b39f29ff0f3905419bb8f47"
SRC_URI[bananapi-m5-ddr4_1d.fw.sha256sum] = "2a7b3197699b3991021df2432ad940b58c435d6627988e7c4450ea9300854051"
SRC_URI[bananapi-m5-ddr4_2d.fw.sha256sum] = "b57a75959a24fd6de6a2ed80f7e8a7cd6911c78b361de86a7e4f194b9f950ae0"
SRC_URI[bananapi-m5-diag_lpddr4.fw.sha256sum] = "0945f0cdf8b33099398d550b520741acbef0532032b88bb4359e2b9a4989ac8b"
SRC_URI[bananapi-m5-lpddr3_1d.fw.sha256sum] = "4ff0f3e3cc20bbf3ed2d68ca6439fc1ff19b433c2e9fa1a1bcebaaedaa5a9cc7"
SRC_URI[bananapi-m5-lpddr4_1d.fw.sha256sum] = "964d534d6f2f8bf20bab691463c43657da6bdbfa7e8801dde902e644091d0e74"
SRC_URI[bananapi-m5-lpddr4_2d.fw.sha256sum] = "9dd80a91546d61148d4f5712f1d12e3f7641987a6642512986cc56a17c4fc3c6"
SRC_URI[bananapi-m5-piei.fw.sha256sum] = "c2240732a217ac61ed53243668e7247a323fc5ea1450383ff62a55092136af16"

pkgdatadir = "${datadir}/${BPN}"

do_fetch[cleandirs] += "${WORKDIR}/_fw"

do_install() {
    cd '${WORKDIR}/_fw'
    for i in *; do
        base=$i
        install -D -p -m 0644 "$i" '${D}${pkgdatadir}'/"$base"
    done
}
