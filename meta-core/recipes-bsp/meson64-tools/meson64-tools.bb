LICENSE = "CLOSED"

SRC_URI = "\
    git+https://github.com/angerman/meson64-tools.git \
    file://system-libraries.patch \
    file://misc-fixes.patch \
    file://mbedtls-recent.patch \
"

SRCREV = "b09cefd1e001dbba14036857bf6e167bf1833f26"

BBCLASSEXTEND = "native nativesdk"

inherit autotools-brokensep

S = "${WORKDIR}/git"
B = "${S}"

DEPENDS += "mbedtls lz4"

EXTRA_OEMAKE += "\
    CC='${CC}' \
    CFLAGS='${CFLAGS}' \
    LDFLAGS='${LDFLAGS} -lmbedcrypto -llz4' \
    PREFIX='${D}${pkglibexecdir}' \
"

pkglibexecdir = "${libexecdir}/${BPN}"
