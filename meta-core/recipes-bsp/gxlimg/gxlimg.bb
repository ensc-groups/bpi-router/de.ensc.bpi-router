LICENSE = "BSD-2-Clause"
LIC_FILES_CHKSUM = "file://LICENSE;md5=de09d893c458f4d78748b1c40e1b6916"

SRC_URI += "\
    git+https://github.com/repk/gxlimg.git \
    file://blx-fix.sh \
"

SRCREV = "504af7e4f33a004835c84f3f316e3995193bfc7a"

BBCLASSEXTEND = "native nativesdk"

inherit autotools-brokensep

S = "${WORKDIR}/git"
B = "${S}"

DEPENDS += "openssl"

EXTRA_OEMAKE += "\
    CC='${CC}' \
    CFLAGS='${CFLAGS}' \
    LDFLAGS='${LDFLAGS} -lssl -lcrypto' \
"

do_install() {
    install -D -p -m 0755 gxlimg ${D}${bindir}/gxlimg
    install -D -p -m 0755 ${WORKDIR}/blx-fix.sh '${D}/${bindir}'/blx-fix.sh
}
