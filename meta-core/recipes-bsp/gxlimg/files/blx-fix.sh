#! /bin/bash

## Usage: $0 <type> <blx0.bin> <blx01.bin> <output>

case $1 in
  bl30)
	limit_blx=40960
	limit_blx01=13312
	;;
  bl2)
	limit_blx=57344
	limit_blx01=4096
	;;
  *)
	echo "Invalid type '$1'" >&2
	exit 1
esac

_truncate() {
    dd if=/dev/zero of="$2" bs="$1" count=0 seek=1 status=none
}

set -e

d=`mktemp -t -d blx-fix.XXXXXX` || exit
trap "rm -rf $d" EXIT

cp "$2" "$d/blx"
cp "$3" "$d/blx01"

_truncate "$limit_blx"   "$d/blx"
_truncate "$limit_blx01" "$d/blx01"

rm -f "$4"

cat "$d/blx" "$d/blx01" > "$4"
