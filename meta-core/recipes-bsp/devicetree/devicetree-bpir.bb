FILESEXTRAPATHS:prepend := "${LAYERDIR_de.ensc.bpi-router.core}/../files/dtree:"

SRC_URI_DTB = "\
    file://${MACHINE}.dts \
"

SRC_URI = "\
    ${SRC_URI_DTB} \
\
    ${MACHINE_DTB_SOURCES} \
"

MACHINE_DTB_SOURCES[vardeps] += "MACHINE_DTBS"
MACHINE_DTB_SOURCES = "${@machine_dtbs_to_src_uri(d.getVar('MACHINE_DTBS'))}"

COMPATIBLE_MACHINE  = "bananapi-m5"
COMPATIBLE_MACHINE .= "|bananapi-m2b"
COMPATIBLE_MACHINE .= "|bananapi-m1"
COMPATIBLE_MACHINE .= "|bananapi-router"

inherit devicetree

def machine_dtbs_to_src_uri(dtbs):
    res = []
    for d in sorted((dtbs or "").split()):
        if d[-4:] == '.dtb':
            f = d[:-4]
        elif d[-5:] == '.dtbo':
            f = d[:-5]
        else:
            raise Exception("unsupported dtb name '%s'" % d)

        res.append('file://%s.dts' % f)

    return ' '.join(res)
