LICENSE = "GPL-3.0-or-later"
LIC_FILES_CHKSUM="file://COPYING;md5=1ebbd3e34237af26da5dc08a4e440464"

SRC_URI = "\
    ${URI_PROJECT_TOOLS}/ppp-notify;branch=master \
"
SRCREV = "9eb56c9b850ecc467dc183d5247ffde935c74122"

S = "${WORKDIR}/git"

inherit autotools systemd

EXTRA_OEMAKE = "\
  -f ${S}/Makefile \
"

SYSTEMD_SERVICE:${PN} = "ppp-eventd.socket"
