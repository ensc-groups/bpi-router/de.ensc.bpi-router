FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "\
  ${@bb.utils.contains('PACKAGECONFIG', 'selinux', 'file://selinux.cfg', '', d)} \
  file://bpi-setup.cfg \
"

PACKAGECONFIG:append = " selinux"

PACKAGECONFIG[selinux] = ",,libselinux"

SYSTEMD_SERVICE:${PN}-syslog = ""
