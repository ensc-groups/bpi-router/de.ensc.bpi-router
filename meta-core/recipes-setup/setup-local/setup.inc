LICENSE = "CLOSED"

PACKAGE_ARCH = "${MACHINE_ARCH}"

BRANCH = "common/next"
SRCREV = "XXX"

SRC_URI = "\
    ${URI_PROJECT_PRIV}/setup-local.git;branch=${BRANCH};name=git \
"

S = "${WORKDIR}/git"

DEPENDS += "iptables-builder-native rsync-native"

EXTRA_OEMAKE += "\
    ROLE='${ROLE}' \
"

BBCLASSEXTEND += "native nativesdk"

ALLOW_EMPTY:${PN} = "1"

RDEPENDS:${PN} += "setup-helper"
