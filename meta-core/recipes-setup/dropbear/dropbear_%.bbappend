dropbear_add_persistkey() {
	sed -i \
		-e '\|ConditionPathExists=!/var/lib|a\' \
	        -e 'ConditionPathExists=!/persist/dropbear/dropbear_rsa_host_key\' \
		-e 'After=persist.mount' \
\
		-e '/^RemainAfterExit=/a\' \
		-e 'SELinuxContext=system_u:system_r:dropbearkeygen_t:s0' \
\
		${WORKDIR}/dropbearkey.service
}
do_unpack[postfuncs] += "dropbear_add_persistkey"
