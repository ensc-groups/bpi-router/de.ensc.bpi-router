LICENSE = "CLOSED"

SRC_URI = "\
    file://dhcp-setup.service \
    file://dhcp-empty.conf \
"

inherit systemd

do_install() {
	install -D -p -m 0644 ${WORKDIR}/dhcp-empty.conf    ${D}${sysconfdir}/dhcp/dhcp-empty.conf
	install -D -p -m 0644 ${WORKDIR}/dhcp-setup.service ${D}${systemd_system_unitdir}/dhcp-setup.service
}

SYSTEMD_SERVICE:${PN} = "dhcp-setup.service"

RDEPENDS:${PN} += "setup-helper"
