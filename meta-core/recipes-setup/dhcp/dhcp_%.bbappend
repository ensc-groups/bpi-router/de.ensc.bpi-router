SYSTEMD_AUTO_ENABLE:${PN}-server = "enable"

SYSTEMD_SERVICE:${PN}-server:remove = "dhcpd6.service"

do_install:append() {
	rm -f ${D}${systemd_unitdir}/system/dhcpd6.service
}
