FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "\
    file://persist.patch \
    file://named-setup-rndc.service \
"

do_compile:append() {
	rm -f named-setup-rndc.service
	sed -e 's,@BASE_BINDIR@,${base_bindir},g' \
	    -e 's,@SBINDIR@,${sbindir},g' \
		${WORKDIR}/named-setup-rndc.service > named-setup-rndc.service
}

do_install:append() {
	install -p -m 0644 named-setup-rndc.service ${D}${systemd_system_unitdir}/named-setup-rndc.service
}

FILES:${PN} += "${systemd_system_unitdir}/named-setup-rndc.service"
RRECOMMENDS:${PN} += "bind-setup"
