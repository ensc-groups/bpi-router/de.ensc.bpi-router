LICENSE = "CLOSED"

SRC_URI = "\
    file://generate-hosts \
"

do_install() {
	install -p -D -m 0755 ${WORKDIR}/generate-hosts ${D}${bindir}/generate-hosts
}

BBCLASSEXTEND = "native nativesdk"

RDEPENDS:${PN} += "bash"
