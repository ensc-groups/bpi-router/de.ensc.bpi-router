do_install:append() {
	rm -f ${D}${sysconfdir}/sysctl.d/99-sysctl.conf ${D}${sysconfdir}/sysctl.conf
}

CONFFILES:${PN}:remove = "${D}${sysconfdir}/sysctl.conf"
