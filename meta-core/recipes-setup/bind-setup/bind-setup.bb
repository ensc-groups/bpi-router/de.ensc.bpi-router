LICENSE = "CLOSED"

SRC_URI = "\
  file://bind-setup.service \
  file://bind-empty.conf \
"

inherit systemd

do_install() {
	install -D -p -m 0644 ${WORKDIR}/bind-empty.conf    ${D}${sysconfdir}/bind/bind-empty.conf
	install -D -p -m 0644 ${WORKDIR}/bind-setup.service ${D}${systemd_system_unitdir}/bind-setup.service
}

SYSTEMD_SERVICE:${PN} = "bind-setup.service"

RDEPENDS:${PN} += "setup-helper gnupg"
