LICENSE = "CLOSED"

PACKAGE_ARCH = "${MACHINE_ARCH}"

BRANCH = "common/next"
SRCREV = "XXX"

SRC_URI = "\
    ${URI_PROJECT_TOOLS}/setup.git;branch=${BRANCH} \
    file://prepare-persist-${ROLE}.service \
    file://relabel-machine-id.service \
    file://iptables-default@.service \
    file://iptables-persist@.service \
    file://iptables.target \
    file://iptables-setup-default.service \
    file://iptables-setup-persist.service \
    file://iptables-setup.target \
    file://systemd-system.conf \
"

S = "${WORKDIR}/git"

DEPENDS += "git-native iptables-builder-native"

SYSTEMD_SERVICE:${PN} = "\
    prepare-persist.service \
    relabel-machine-id.service \
    iptables-persist@ipv4.service \
    iptables-persist@ipv6.service \
    iptables-default@ipv4.service \
    iptables-default@ipv6.service \
    iptables.target \
    iptables-setup-default.service \
    iptables-setup-persist.service \
    iptables-setup.target \
"

EXTRA_OEMAKE += "\
    ROLE='${ROLE}' \
"

inherit systemd update-alternatives

install_unit() {
	install -D -p -m 0644 ${WORKDIR}/$1 ${D}${systemd_system_unitdir}/${2:-$1}
}

do_install() {
	oe_runmake install DESTDIR=${D}

	install -D -p -m 0644 ${WORKDIR}/systemd-system.conf ${D}${sysconfdir}/systemd/system.conf.d/98-local.conf

	install_unit prepare-persist-${ROLE}.service prepare-persist.service
	install_unit relabel-machine-id.service

	install_unit iptables-default@.service
	install_unit iptables-persist@.service
	install_unit iptables.target

	install_unit iptables-setup-default.service
	install_unit iptables-setup-persist.service
	install_unit iptables-setup.target
}

FILES:${PN} += "${systemd_system_unitdir}/*"
RDEPENDS:${PN} += "bash iptables-nft util-linux-flock setup-helper"

ALTERNATIVE:${PN} = "resolv-conf"
ALTERNATIVE_TARGET[resolv-conf] = "${sysconfdir}/resolv-conf.setup"
ALTERNATIVE_LINK_NAME[resolv-conf] = "${sysconfdir}/resolv.conf"
ALTERNATIVE_PRIORITY[resolv-conf] ?= "99"
