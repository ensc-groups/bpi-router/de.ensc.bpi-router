ROLE = "router"

require setup.inc

SRC_URI += "\
    file://openvpn-client-reload@.service \
    file://openvpn-client-reload@.timer \
    file://ppp-reload@.service \
    file://ppp-reload@.timer \
    file://tc@.service \
"

DEPENDS += "host-builder-native"

do_install:append() {
	install_unit openvpn-client-reload@.service
	install_unit openvpn-client-reload@.timer

	install_unit ppp-reload@.service
	install_unit ppp-reload@.timer

	install_unit tc@.service
}
