ROLE = "server"

require setup.inc

SRC_URI += "\
"

DEPENDS += "host-builder-native"

RPROVIDES:${PN} += "virtual/nss-ldap-config"
RCONFLICS:${PN} += "nss-pam-ldapd-config"
