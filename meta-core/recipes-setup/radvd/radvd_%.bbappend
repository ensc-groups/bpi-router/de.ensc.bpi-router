## base recipe uses generic 'nogroup'; create a dedicated one
GROUPADD_PARAM:${PN} = "--system radvd"
USERADD_PARAM:${PN} = "--system --home ${localstatedir}/run/radvd/ -M -g radvd radvd"

SYSTEMD_AUTO_ENABLE = "enable"

do_install:append() {
    rm -f ${D}${sysconfdir}/radvd.conf
}
