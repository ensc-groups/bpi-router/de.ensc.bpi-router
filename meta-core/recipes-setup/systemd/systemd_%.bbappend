FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "\
    file://wait-online-quiet.patch \
"

PACKAGECONFIG:remove = "\
    backlight \
    firstboot \
    hibernate \
    hostnamed \
    idn \
    iptc \
    libidn \
    libidn2 \
    localed \
    localed \
    machined \
    myhostname \
    nss-mymachines \
    nss-resolve \
    polkit \
    quotacheck \
    resolved \
    rfkill \
    smack \
    sysusers \
    sysvcompat \
    sysvinit \
    userdb \
"

PACKAGECONFIG:append = "\
    c-locale \
    cgroupv2 \
    journal-color \
    journal-upload \
    logind \
    microhttpd \
    selinux \
"

PACKAGECONFIG[sysvcompat] = ", -Dsysvinit-path= -Dsysvrcnd-path="
PACKAGECONFIG[c-locale] = " -Ddefault-locale=C"

RRECOMMENDS:${PN}:remove = "systemd-compat-units"
