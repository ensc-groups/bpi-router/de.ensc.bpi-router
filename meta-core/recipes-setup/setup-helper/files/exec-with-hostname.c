#define _GNU_SOURCE	1

#include <stdio.h>
#include <sysexits.h>
#include <sched.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	int		rc;

	if (argc < 2)
		return EX_USAGE;

	rc = unshare(CLONE_NEWUTS);
	if (rc < 0) {
		perror("unshare()");
		return EX_OSERR;
	}

	rc = sethostname(argv[1], strlen(argv[1]));
	if (rc < 0) {
		perror("sethostname()");
		return EX_OSERR;
	}

	execv(argv[2], argv + 2);

	perror("execve()");
	return EX_OSFILE;
}
