#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>

int main(int argc, char *argv[])
{
	char const	*cfg_file = argv[1];
	char const	*test_file = argv[2];
	char const	*yes_file = argv[3];
	char const	*no_file = argv[4];

	char const	*dst_file;

	struct stat	st;
	int		rc;

	dst_file = lstat(test_file, &st) < 0 ? no_file : yes_file;

	unlink(cfg_file);
	rc = symlink(dst_file, cfg_file);
	if (rc < 0) {
		perror("symlink()");
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
