LICENSE = "CLOSED"

SRC_URI = "\
    file://setup-symlink.c \
    file://exec-with-hostname.c \
"

do_compile() {
	${CC} ${CFLAGS} ${LDFLAGS} ${WORKDIR}/setup-symlink.c      -o setup-symlink
	${CC} ${CFLAGS} ${LDFLAGS} ${WORKDIR}/exec-with-hostname.c -o exec-with-hostname
}

do_install() {
	install -D -p -m 0755 setup-symlink      ${D}${sbindir}/setup-symlink
	install -D -p -m 0755 exec-with-hostname ${D}${sbindir}/exec-with-hostname
}
